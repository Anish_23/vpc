provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_vpc" "main" {
  cidr_block = var.main_cidr_block
  tags = {
    "AWSServiceAccount" = "697148468905"
  }
  tags_all = {
    "AWSServiceAccount" = "697148468905"
  }
}
