resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.aws_subnet_main_cidr
  tags = {
    "AWSServiceAccount" = "697148468905"
  }
  tags_all = {
    "AWSServiceAccount" = "697148468905"
  }
}

resource "aws_subnet" "main2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.aws_subnet_main2_cidr
  tags = {
    "AWSServiceAccount" = "697148468905"
  }
  tags_all = {
    "AWSServiceAccount" = "697148468905"
  }
}
