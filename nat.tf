resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_nat_gateway" "Deafult-NAT" {
  allocation_id = aws_eip.eip.id
  subnet_id     = "subnet-046e9325"
  tags = {
    "Name" = "Deafult-NAT"
  }
}
