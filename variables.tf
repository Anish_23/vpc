variable "profile" {
  default     = "default"
  type        = string
  description = "AWS Profile"
}

variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Define the AWS Region"
}

variable "main_cidr_block" {
  default     = "172.16.0.0/16"
  type        = string
  description = "Define the cidr block for the main vpc"
}

variable "aws_subnet_main_cidr" {
  default     = "172.16.1.0/24"
  type        = string
  description = "Cidr block for main subnet"
}

variable "aws_subnet_main2_cidr" {
  default     = "172.16.0.0/24"
  type        = string
  description = "Cidr block for main2 subnet"
}
