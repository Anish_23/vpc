resource "aws_route_table" "route_1" {
  vpc_id = aws_vpc.main.id
  route = [
    {
      carrier_gateway_id         = ""
      cidr_block                 = "0.0.0.0/0"
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = aws_internet_gateway.gw.id
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      nat_gateway_id             = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]
  tags = {
    "AWSServiceAccount" = "697148468905"
  }
}

resource "aws_route_table" "route_2" {
  vpc_id = aws_vpc.main.id

}

resource "aws_route_table_association" "a" {
  route_table_id = aws_route_table.route_1.id
  subnet_id      = aws_subnet.main.id
}

resource "aws_route_table_association" "b" {
  route_table_id = aws_route_table.route_1.id
  subnet_id      = aws_subnet.main2.id
}
